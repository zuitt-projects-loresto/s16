function displayMsgToSelf() {
	console.log("Hello")
}

let count = 10;

while (count !== 0) {
	displayMsgToSelf()
	count--;
}

// While loop
	// While loop will allow us to repeat an instruction as long as the condition is true

let number = 5
// countdown from 5 to 1
while(number !== 0) {
	console.log("While " + number);
	number--;
}

//count from 0 to 10
while(number <= 10) {
	console.log(`While ${number}`);
	number++;
}

//Do-While Loop
	// A do-while loop works a lot like a while loop but unlike while loop, do-while guarantees to run at least once


// Number function works like parseInt, it changes a string type to a number data type.
let number2 = Number(prompt("Give me a number"))

do{
	console.log(`Do While ${number2}`);

	number2 += 1;

} while (number2 < 10)

let counter = 1

do {
	console.log(counter);
	counter ++;

} while (counter <=21)

// For Loop
	// A more flexible loop than while and do-while
	// It consists of three parts: initialization, condition, finalExpression
		// initialization - determines where the loop starts
		// condition - determines when the loop stops
		// finalExpression - indicates how to advance the loop (incrementation or decrementation)

for(let count = 0; count <= 20; count ++){
	console.log(count)
};

let myString = "alex"
console.log(myString.length)
	// result: 4
// note: strings are special compared to other data types
	// it has access to functions and other pieces of information another primitive might not have

	console.log(myString[0]);
		// result: a
	console.log(myString[3]);
		// result: x

	for(let x = 0; x < myString.length; x++) {
		console.log(myString[x]);
	}

	let myName = "Jessa Mae"

	for(let i = 0; i < myName.length; i++){

		if(
			myName[i].toLowerCase() == "a" ||
			myName[i].toLowerCase() == "e" ||
			myName[i].toLowerCase() == "i" ||
			myName[i].toLowerCase() == "o" ||
			myName[i].toLowerCase() == "u"
		){
			console.log(3)
		} else {
			console.log(myName[i])
		}
	};

// Continue and Break Statements

	for(let count = 0; count <= 20; count ++) {

		if(count % 2 === 0) {
			continue;
		}

		console.log(`Continue and Break ${count}`)

		if(count > 10){
			break;
		}
	};

	let name = "Andrea"

	for(let i = 0; i < name.length; i++){
		console.log(name[i]);

		if(name[i].toLowerCase() === "a"){
			console.log("Continue to the next iteration")
			continue;
		}
		if(name[i] == "d"){
			break;
		}
	};